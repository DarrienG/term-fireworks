all:
	cargo build --release
	strip target/release/fireworks

check:
	cargo clean
	cargo clippy --all -- -D warnings
	cargo test
