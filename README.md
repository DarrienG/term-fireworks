# Fireworks

Fireworks is a simple firework simulation written for the terminal.

Watch a demo [here](https://asciinema.org/a/357083) or download the binary
[here](https://gitlab.com/DarrienG/term-fireworks/-/releases)
if you want to jump right in!

You can also do a: `cargo install fireworks` if you do rust development.

Note: We only support Linux and macOS.

## Usage

It's really quite simple, just run:

```
fireworks
```

and you're done :)

The fireworks are generated in a deterministic way (despite an element of
randomness), so if you have a favorite firework pattern, you can run with a
specific seed like so:

```
$ fireworks --seed 8675309
```

## Contributing

Interested in contributing to the fireworks demo? We're always happy to take
contributions!  We require tests for all new features and fixes. All code must
also be formatted by rustfmt and the strictest clippy lints.

## Extras

The development of this was blogged
[here](https://blog.darrien.dev/posts/fireworks-for-your-terminal/) if you want
to know how it was made or want to make some fireworks of your own :)
