use firework::types::Drawables;
use std::{
  sync::mpsc::{Receiver, SyncSender},
  thread,
};

use crate::firework;

mod state_machine;

pub fn start(seed: u64, regen_buffer_filler: SyncSender<Drawables>, end_signal: Receiver<bool>) {
  thread::spawn(move || compositor_loop(seed, regen_buffer_filler, end_signal));
}

pub fn compositor_loop(
  seed: u64,
  regen_buffer_filler: SyncSender<Drawables>,
  end_signal: Receiver<bool>,
) {
  let mut state_machine = state_machine::StateMachine::new(seed, terminal_width());

  loop {
    let drawables = state_machine.tick(terminal_width());
    if regen_buffer_filler.send(drawables).is_err() {
      panic!("Renderer unexpectedly died!");
    }
    if let Ok(v) = end_signal.try_recv() {
      if v {
        return;
      }
    }
  }
}

#[inline]
fn terminal_width() -> u16 {
  termion::terminal_size()
    .expect("Unable to get terminal size")
    .1
}
