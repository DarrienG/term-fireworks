use drawing::{
  colors,
  colors::{FireworkType, PixelColor},
  Drawable, Point,
};
use dyn_clone::DynClone;
use rand::rngs::StdRng;
use rand::Rng;
use simple::{SimpleExplodingPoints, SimpleFallingPoints, SimpleTailPoints};
use std::vec::IntoIter;
use types::Drawables;
mod optimizer;

pub mod drawing;
pub mod simple;
pub mod types;

trait Eraseable {
  fn erase(&mut self);
}

pub trait Memoried {
  fn remember(&mut self);
}

trait FireworkComponent: Send + Sync + Eraseable + Memoried + Drawable + DynClone {
  fn as_drawable(self: Box<Self>) -> Box<dyn Drawable>;
}

dyn_clone::clone_trait_object!(FireworkComponent);

trait TailPoints: FireworkComponent {
  fn new(center: Point) -> Box<Self>
  where
    Self: Sized;
  fn grow(&mut self, center: &Point, pixel_color: PixelColor);
  fn shrink(&mut self);
}

trait ExplodingPoints: FireworkComponent {
  fn new() -> Box<Self>
  where
    Self: Sized;
  fn explode(&mut self, center: &Point, pixel_color: PixelColor);
  fn collapse(&mut self);
  fn get_perpendicular_tips(&self) -> Vec<Point>;
  fn get_diagonal_tips(&self) -> Vec<Point>;
}

trait FallingPoints: FireworkComponent {
  fn new() -> Box<Self>
  where
    Self: Sized;
  fn fall(
    &mut self,
    perpendicular_tips: &[Point],
    diagonal_tips: &[Point],
    pixel_color: PixelColor,
  );
  fn fade(&mut self);
}

#[derive(PartialEq, Copy, Clone, Debug)]
pub enum FireworkState {
  Flying,    // Just a tail - going up
  Exploding, // Bursting outwards
  Falling,   // bursting out has stopped, fall from the tips and fade from center to tips
  Fading,    // tips disappear to fading at 2x the rate fade falls
  Gone,      // nothing, a blank canvas, the inner machinations of my mind
}

impl FireworkState {
  pub fn ordinal(&self) -> u8 {
    match self {
      FireworkState::Flying => 0,
      FireworkState::Exploding => 1,
      FireworkState::Falling => 2,
      FireworkState::Fading => 3,
      FireworkState::Gone => 25,
    }
  }
}

pub struct Firework {
  total_state: IntoIter<FireworkState>,
  center: Center,
  tail_points: Box<dyn TailPoints>,
  exploding_points: Box<dyn ExplodingPoints>,
  falling_points: Box<dyn FallingPoints>,
  extinguished: bool,
  firework_type: FireworkType,
}

impl Firework {
  pub fn new(mut rng: StdRng, term_height: u16) -> Self {
    let firework_type = colors::pick_firework_type(&mut rng);
    let center = Center::new(&mut rng);
    let center_point = center.position;
    Firework {
      total_state: Firework::build_total_state(&mut rng, term_height).into_iter(),
      center,
      tail_points: SimpleTailPoints::new(center_point),
      exploding_points: SimpleExplodingPoints::new(),
      falling_points: SimpleFallingPoints::new(),
      extinguished: false,
      firework_type,
    }
  }

  pub fn advance(&mut self) -> FireworkState {
    let state = self.total_state.next();
    if let Some(s) = state {
      match s {
        FireworkState::Flying => self.advance_flying(),
        FireworkState::Exploding => self.advance_exploding(),
        FireworkState::Falling => self.advance_falling(),
        FireworkState::Fading => self.advance_fading(),
        FireworkState::Gone => self.advance_gone(),
      }
      return s;
    };
    FireworkState::Gone
  }

  pub fn drawables(&self) -> Drawables {
    vec![
      dyn_clone::clone_box(&*self.tail_points).as_drawable(),
      dyn_clone::clone_box(&*self.exploding_points).as_drawable(),
      dyn_clone::clone_box(&*self.falling_points).as_drawable(),
    ]
    .into_iter()
    .map(optimizer::optimize)
    .collect::<Vec<Box<dyn Drawable>>>()
  }

  pub fn extinguished(&self) -> bool {
    self.extinguished
  }

  fn build_total_state(rng: &mut StdRng, term_height: u16) -> Vec<FireworkState> {
    let mut total_state = vec![];
    for state in vec![
      FireworkState::Flying,
      FireworkState::Exploding,
      FireworkState::Falling,
      FireworkState::Fading,
      FireworkState::Gone,
    ]
    .into_iter()
    {
      let mut advance = true;
      let mut threshold = 0;
      while advance {
        if state == FireworkState::Gone {
          // we only need one
          total_state.push(state);
          break;
        }
        if rng.gen_range(1, Firework::advance_threshold(state)) > threshold {
          total_state.push(state);
          threshold += match term_height {
            0..=24 => 11,
            25..=32 => 7,
            _ => 5,
          };
        } else {
          advance = false;
        }
      }
    }
    total_state
  }
  fn advance_flying(&mut self) {
    self.tail_points.remember();

    self.center.update_center(0, 1);
    self.tail_points.grow(
      &self.center.position,
      self.firework_type.derive_color(FireworkState::Flying),
    );
  }
  fn advance_exploding(&mut self) {
    self.tail_points.remember();
    self.exploding_points.remember();

    self.tail_points.shrink();
    self.exploding_points.explode(
      &self.center.position,
      self.firework_type.derive_color(FireworkState::Exploding),
    );
  }
  fn advance_falling(&mut self) {
    self.tail_points.remember();
    self.exploding_points.remember();
    self.falling_points.remember();

    self.tail_points.erase();
    self.exploding_points.collapse();
    self.falling_points.fall(
      &self.exploding_points.get_diagonal_tips(),
      &self.exploding_points.get_perpendicular_tips(),
      self.firework_type.derive_color(FireworkState::Falling),
    );
  }
  fn advance_fading(&mut self) {
    self.exploding_points.remember();
    self.falling_points.remember();

    self.exploding_points.erase();
    self.falling_points.fade();
  }
  fn advance_gone(&mut self) {
    self.tail_points.remember();
    self.exploding_points.remember();
    self.falling_points.remember();

    self.tail_points.erase();
    self.exploding_points.erase();
    self.falling_points.erase();

    self.extinguished = true;
  }
  fn advance_threshold(state: FireworkState) -> u64 {
    match state {
      FireworkState::Flying => 1000,
      FireworkState::Exploding => 500,
      FireworkState::Falling => 200,
      FireworkState::Fading => 200,
      FireworkState::Gone => 1,
    }
  }
}

#[derive(Debug)]
struct Center {
  position: Point,
}

impl Center {
  fn new(rng: &mut StdRng) -> Self {
    let x = rng.gen_range(0, u16::MAX);
    Center {
      position: Point {
        x,
        y: 0,
        pixel_color: Default::default(),
      },
    }
  }

  fn update_center(&mut self, x_move: i16, y_move: i16) {
    self.position = self.position.move_point(x_move, y_move)
  }
}
