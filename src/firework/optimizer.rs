use firework::drawing::{Drawable, Point};
use std::collections::HashSet;

use crate::firework;

pub struct SimpleDrawable {
  to_draw: Vec<Point>,
  to_clear: Vec<Point>,
}

impl Drawable for SimpleDrawable {
  fn draw(&self) -> Vec<Point> {
    self.to_draw.clone()
  }
  fn clear(&self) -> Vec<Point> {
    self.to_clear.clone()
  }
}

pub fn optimize(drawable: Box<dyn Drawable>) -> Box<dyn Drawable> {
  Box::new(SimpleDrawable {
    to_draw: difference(&drawable.draw(), &drawable.clear()),
    to_clear: difference(&drawable.clear(), &drawable.draw()),
  })
}

fn difference(points_1: &[Point], points_2: &[Point]) -> Vec<Point> {
  let set_1 = points_1.iter().cloned().collect::<HashSet<Point>>();

  let set_2 = points_2.iter().cloned().collect::<HashSet<Point>>();
  set_1.difference(&set_2).cloned().collect()
}
