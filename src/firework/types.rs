use firework::drawing::Drawable;

use crate::firework;

pub type Drawables = Vec<Box<dyn Drawable>>;
