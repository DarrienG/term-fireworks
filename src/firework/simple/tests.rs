use super::firework::{Firework, FireworkState};

const SEED: u64 = 8675309;
const TERM_HEIGHT: u16 = 24;

#[test]
fn test_gen_state_constant() {
  // make sure it's consistent every time with a seeed
  let rng = rand::SeedableRng::seed_from_u64(SEED);
  let state_machine = Firework::new(rng, TERM_HEIGHT);

  let initial_state = state_machine.total_state.collect::<Vec<FireworkState>>();
  for _ in 0..100 {
    let rng = rand::SeedableRng::seed_from_u64(SEED);
    let state_machine = Firework::new(rng, TERM_HEIGHT);

    assert_eq!(
      state_machine.total_state.collect::<Vec<FireworkState>>(),
      initial_state
    );
  }
}
