use colors::PixelColor;
use std::convert::TryInto;

pub mod colors;

pub trait Drawable: Send + Sync {
  fn draw(&self) -> Vec<Point>;
  fn clear(&self) -> Vec<Point>;
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Point {
  pub x: u16,
  pub y: u16,
  pub pixel_color: PixelColor,
}

impl Point {
  #[must_use]
  pub fn move_point(&self, x_move: i16, y_move: i16) -> Point {
    Point {
      x: Point::move_single_value(self.x, x_move),
      y: Point::move_single_value(self.y, y_move),
      pixel_color: self.pixel_color,
    }
  }
  #[must_use]
  pub fn with_pixel_color(&self, pixel_color: PixelColor) -> Point {
    Point {
      pixel_color,
      ..*self
    }
  }
  fn move_single_value(initial: u16, move_val: i16) -> u16 {
    let fail_msg = "Illegal operation - could not convert to u16";
    if move_val > 0 {
      initial.saturating_add(move_val.abs().try_into().expect(fail_msg))
    } else {
      initial.saturating_sub(move_val.abs().try_into().expect(fail_msg))
    }
  }
}
