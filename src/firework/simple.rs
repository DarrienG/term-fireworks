use firework::{
  drawing::{colors::PixelColor, Drawable, Point},
  Eraseable, ExplodingPoints, FallingPoints, FireworkComponent, Memoried, TailPoints,
};
use std::{cmp, default::Default};

use crate::firework;

#[cfg(test)]
mod tests;

#[derive(Clone, Debug)]
pub struct SimpleTailPoints {
  tail: Vec<Point>,
  old_tail: Vec<Point>,
}

impl TailPoints for SimpleTailPoints {
  fn new(center: Point) -> Box<Self> {
    Box::new(SimpleTailPoints {
      tail: vec![center],
      old_tail: Default::default(),
    })
  }

  fn grow(&mut self, center: &Point, pixel_color: PixelColor) {
    let tail_size = self.tail.len();
    self.tail.clear();
    for i in 0..tail_size + 1 {
      self.tail.push(
        center
          .move_point(0, -(i as i16))
          .with_pixel_color(pixel_color),
      )
    }
  }
  fn shrink(&mut self) {
    if self.tail.len() != 1 {
      self.tail.pop();
    }
  }
}

impl FireworkComponent for SimpleTailPoints {
  fn as_drawable(self: Box<Self>) -> Box<dyn Drawable> {
    self
  }
}

impl Eraseable for SimpleTailPoints {
  fn erase(&mut self) {
    self.tail.clear();
  }
}

impl Drawable for SimpleTailPoints {
  fn draw(&self) -> Vec<Point> {
    self.tail.clone()
  }
  fn clear(&self) -> Vec<Point> {
    self.old_tail.clone()
  }
}

impl Memoried for SimpleTailPoints {
  fn remember(&mut self) {
    self.old_tail = self.tail.clone();
  }
}

#[derive(Clone, Default, Debug)]
pub struct SimpleExplodingPoints {
  explosion_iter: u8,
  perpendicular_tips: Vec<Point>,
  diagonal_tips: Vec<Point>,
  explosion: Vec<Point>,
  old_explosion: Vec<Point>,
}

impl SimpleExplodingPoints {
  fn explode_perpendicular(&mut self, center: &Point, pixel_color: PixelColor) {
    // ↑↓←→
    let up = center
      .move_point(0, self.explosion_iter.into())
      .with_pixel_color(pixel_color);
    let down = center
      .move_point(0, -(self.explosion_iter as i16))
      .with_pixel_color(pixel_color);
    let left = center
      .move_point(self.explosion_iter as i16, 0)
      .with_pixel_color(pixel_color);
    let right = center
      .move_point(-(self.explosion_iter as i16), 0)
      .with_pixel_color(pixel_color);

    self.perpendicular_tips.push(up);
    self.perpendicular_tips.push(down);
    self.perpendicular_tips.push(left);
    self.perpendicular_tips.push(right);

    self.explosion.push(up);
    self.explosion.push(down);
    self.explosion.push(left);
    self.explosion.push(right);
  }
  fn maybe_explode_diagonal(&mut self, center: &Point, pixel_color: PixelColor) {
    if self.explosion_iter % 2 == 0 {
      self.diagonal_tips.clear();
      // diagonal points should go out half as quickly
      let move_val = self.explosion_iter / 2;
      // ⬉⬈⬋⬊
      let up_left = center
        .move_point(-(move_val as i16), move_val.into())
        .with_pixel_color(pixel_color);
      let up_right = center
        .move_point(move_val.into(), move_val.into())
        .with_pixel_color(pixel_color);
      let down_left = center
        .move_point(-(move_val as i16), -(move_val as i16))
        .with_pixel_color(pixel_color);
      let down_right = center
        .move_point(move_val.into(), -(move_val as i16))
        .with_pixel_color(pixel_color);

      self.diagonal_tips.push(up_left);
      self.diagonal_tips.push(up_right);
      self.diagonal_tips.push(down_left);
      self.diagonal_tips.push(down_right);

      self.explosion.push(up_left);
      self.explosion.push(up_right);
      self.explosion.push(down_left);
      self.explosion.push(down_right);
    }
  }
}

impl FireworkComponent for SimpleExplodingPoints {
  fn as_drawable(self: Box<Self>) -> Box<dyn Drawable> {
    self
  }
}

impl ExplodingPoints for SimpleExplodingPoints {
  fn new() -> Box<Self> {
    Box::new(Default::default())
  }
  fn explode(&mut self, center: &Point, pixel_color: PixelColor) {
    self.explosion_iter += 1;
    self.perpendicular_tips.clear();
    self.explode_perpendicular(center, pixel_color);
    self.maybe_explode_diagonal(center, pixel_color);
  }
  fn collapse(&mut self) {
    self
      .explosion
      .drain(..(calc_drain_amt(self.explosion_iter, self.explosion.len())));
    self.explosion_iter = self.explosion_iter.saturating_sub(1);
  }
  fn get_diagonal_tips(&self) -> Vec<Point> {
    self.diagonal_tips.clone()
  }
  fn get_perpendicular_tips(&self) -> Vec<Point> {
    self.perpendicular_tips.clone()
  }
}

impl Eraseable for SimpleExplodingPoints {
  fn erase(&mut self) {
    self.explosion.clear();
  }
}

impl Memoried for SimpleExplodingPoints {
  fn remember(&mut self) {
    self.old_explosion = self.explosion.clone();
  }
}

impl Drawable for SimpleExplodingPoints {
  fn draw(&self) -> Vec<Point> {
    self.explosion.clone()
  }
  fn clear(&self) -> Vec<Point> {
    self.old_explosion.clone()
  }
}

#[derive(Debug, Default, Clone)]
pub struct SimpleFallingPoints {
  falling_iter: u8,
  falling: Vec<Point>,
  ready_for_fall: bool,
  old_falling: Vec<Point>,
  falling_tips: Vec<Point>,
}

impl SimpleFallingPoints {
  fn fall_internal(&mut self, falling_tips: &[Point], pixel_color: PixelColor) {
    self.falling_iter += 1;
    falling_tips.iter().for_each(|tip| {
      self.falling.push(
        tip
          .move_point(0, -(self.falling_iter as i16))
          .with_pixel_color(pixel_color),
      )
    });
  }
}

impl FallingPoints for SimpleFallingPoints {
  fn new() -> Box<Self> {
    Box::new(Default::default())
  }
  fn fall(
    &mut self,
    perpendicular_tips: &[Point],
    diagonal_tips: &[Point],
    pixel_color: PixelColor,
  ) {
    self.old_falling = self.falling.clone();
    if self.falling.is_empty() {
      self.falling_tips.extend(
        perpendicular_tips
          .iter()
          .map(|tip| tip.with_pixel_color(pixel_color)),
      );
      self.falling_tips.extend(
        diagonal_tips
          .iter()
          .map(|tip| tip.with_pixel_color(pixel_color)),
      );
    }
    self.fall_internal(perpendicular_tips, pixel_color);
    self.fall_internal(diagonal_tips, pixel_color);
  }
  fn fade(&mut self) {
    if !self.ready_for_fall {
      self.falling_iter = self.falling_iter.saturating_add(1);
      self.ready_for_fall = true;
    }
    self
      .falling
      .drain(..calc_drain_amt(self.falling_iter, self.falling.len()));
    // perhaps fall here too
    self.falling_iter = self.falling_iter.saturating_sub(1);
  }
}

impl FireworkComponent for SimpleFallingPoints {
  fn as_drawable(self: Box<Self>) -> Box<dyn Drawable> {
    self
  }
}

impl Eraseable for SimpleFallingPoints {
  fn erase(&mut self) {
    self.falling.clear();
  }
}

impl Memoried for SimpleFallingPoints {
  fn remember(&mut self) {
    self.old_falling = self.falling.clone();
  }
}

impl Drawable for SimpleFallingPoints {
  fn draw(&self) -> Vec<Point> {
    self.falling.clone()
  }
  fn clear(&self) -> Vec<Point> {
    self.old_falling.clone()
  }
}

fn calc_drain_amt(iteration: u8, drainable_len: usize) -> usize {
  cmp::min(
    match iteration {
      0..=3 => 0,
      _ => {
        if iteration % 2 == 0 {
          8
        } else {
          4
        }
      }
    },
    drainable_len,
  )
}
