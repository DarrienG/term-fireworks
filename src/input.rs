use std::{io::stdin, sync::mpsc::Sender, thread};
use termion::{event::*, input::TermRead};

pub fn capture(senders: Vec<Sender<bool>>) {
  thread::spawn(|| start_input_capture(senders));
}

fn start_input_capture(senders: Vec<Sender<bool>>) {
  for c in stdin().events() {
    let evt = c.expect("Unable to capture stdin event. Exiting.");
    if let Event::Key(Key::Ctrl('c')) = evt {
      for sender in senders {
        if sender.send(true).is_err() {
          println!("Unable to send cancel event to worker! Terminal may look weird.");
        }
      }
      break;
    }
  }
}
