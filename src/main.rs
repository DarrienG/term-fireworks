use clap::{App, Arg};
use firework::types::Drawables;
use rand::Rng;
use std::{
  io::{stdout, Error},
  process::exit,
  sync::{
    mpsc,
    mpsc::{Receiver, Sender, SyncSender},
  },
};
use termion::raw::IntoRawMode;

mod compositor;
pub mod firework;
mod input;
mod renderer;

fn main() -> Result<(), Error> {
  let matches = App::new("Fireworks")
    .version("1.0.4")
    .author("Darrien Glasser <me@darrien.dev>")
    .about("A fun terminal fireworks display")
    .arg(
      Arg::with_name("seed")
        .short("s")
        .long("seed")
        .takes_value(true)
        .help("set custom random seed"),
    )
    .get_matches();

  let seed = if let Some(i) = matches.value_of("seed") {
    i.parse::<u64>().unwrap_or_else(|_| {
      eprintln!("Must be a valid number: {}", i);
      exit(1);
    })
  } else {
    rand::thread_rng().gen_range(0, u64::MAX)
  };

  let (input_sender_1, input_receiver_1): (Sender<bool>, Receiver<bool>) = mpsc::channel();
  let (input_sender_2, input_receiver_2): (Sender<bool>, Receiver<bool>) = mpsc::channel();

  let (regen_buffer_filler, regen_buffer): (SyncSender<Drawables>, Receiver<Drawables>) =
    mpsc::sync_channel(5);

  let mut stdout = stdout()
    .into_raw_mode()
    .expect("Unable to capture stdout. Exiting.");

  input::capture(vec![input_sender_1, input_sender_2]);
  compositor::start(seed, regen_buffer_filler, input_receiver_2);
  renderer::start(&mut stdout, regen_buffer, input_receiver_1);

  Ok(())
}
